package day7

import (
	"cmp"
	_ "embed"
	"slices"
	"strconv"
	"strings"
)

//go:embed input.txt
var input string

var cards = map[rune]int{
	'A': 0,
	'K': 1,
	'Q': 2,
	'J': 3,
	'T': 4,
	'9': 5,
	'8': 6,
	'7': 7,
	'6': 8,
	'5': 9,
	'4': 10,
	'3': 11,
	'2': 12,
}

type Hand struct {
	strength int
	winnings int
}

func Part1() int {
	lines := strings.Split(input, "\r\n")
	hands := make([]Hand, len(lines))
	for i, line := range lines {
		parts := strings.Split(line, " ")
		strength := 0
		occurrences := make([]int, 13)
		for j, c := range parts[0] {
			strength += cards[c] << (4 * (4 - j))
			occurrences[cards[c]]++
		}

		handType := 6
		has3 := false
		has2 := false
		for _, n := range occurrences {
			if n == 5 {
				handType = 0
				break
			} else if n == 4 {
				handType = 1
				break
			} else if n == 3 {
				has3 = true
				handType = 3
			} else if n == 2 {
				if has2 {
					handType = 4
					break
				}
				has2 = true
				handType = 5
			}
		}

		if has3 && has2 {
			handType = 2
		}

		strength += handType << 20
		winnings, _ := strconv.Atoi(parts[1])

		hands[i] = Hand{strength, winnings}
	}

	slices.SortFunc(hands, func(h1 Hand, h2 Hand) int {
		return cmp.Compare(h2.strength, h1.strength)
	})

	total := 0
	for i, hand := range hands {
		total += hand.winnings * (i + 1)
	}

	return total
}

var cardsP2 = map[rune]int{
	'A': 0,
	'K': 1,
	'Q': 2,
	'T': 3,
	'9': 4,
	'8': 5,
	'7': 6,
	'6': 7,
	'5': 8,
	'4': 9,
	'3': 10,
	'2': 11,
	'J': 12,
}

func Part2() int {
	lines := strings.Split(input, "\r\n")
	hands := make([]Hand, len(lines))
	for i, line := range lines {
		parts := strings.Split(line, " ")
		strength := 0
		occurrences := make([]int, 12)
		js := 0
		for j, c := range parts[0] {
			strength += cardsP2[c] << (4 * (4 - j))
			if c == 'J' {
				js++
			} else {
				occurrences[cardsP2[c]]++
			}
		}

		handType := 6
		has3 := false
		has2 := false
		for _, n := range occurrences {
			if n == 5 {
				handType = 0
				break
			} else if n == 4 {
				handType = 1
				break
			} else if n == 3 {
				has3 = true
				handType = 3
			} else if n == 2 {
				if has2 {
					handType = 4
					break
				}
				has2 = true
				handType = 5
			}
		}

		if has3 && has2 {
			handType = 2
		}

		if js == 1 {
			if handType == 1 {
				handType = 0
			} else if handType == 3 {
				handType = 1
			} else if handType == 4 {
				handType = 2
			} else if handType == 5 {
				handType = 3
			} else if handType == 6 {
				handType = 5
			}
		} else if js == 2 {
			if handType == 3 {
				handType = 0
			} else if handType == 5 {
				handType = 1
			} else if handType == 6 {
				handType = 3
			}
		} else if js == 3 {
			if handType == 5 {
				handType = 0
			} else if handType == 6 {
				handType = 1
			}
		} else if js == 4 || js == 5 {
			handType = 0
		}

		strength += handType << 20
		winnings, _ := strconv.Atoi(parts[1])

		hands[i] = Hand{strength, winnings}
	}

	slices.SortFunc(hands, func(h1 Hand, h2 Hand) int {
		return cmp.Compare(h2.strength, h1.strength)
	})

	total := 0
	for i, hand := range hands {
		total += hand.winnings * (i + 1)
	}

	return total
}
