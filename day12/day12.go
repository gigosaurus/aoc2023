package day12

import (
	_ "embed"
	"strconv"
	"strings"
)

//go:embed input.txt
var input string

func Part1() int {
	total := 0
	for _, line := range strings.Split(input, "\r\n") {
		formats := strings.Split(line, " ")
		groupsStr := strings.Split(formats[1], ",")
		groups := make([]int, len(groupsStr))
		for i, str := range groupsStr {
			groups[i], _ = strconv.Atoi(str)
		}

		cache := make([][]int, len(formats[0]))
		for i := range formats[0] {
			cache[i] = make([]int, len(groups))
		}

		total += tryArrangeCache(0, formats[0], 0, groups, &Cache{cache})
	}

	return total
}

func Part2() int {
	total := 0
	for _, line := range strings.Split(input, "\r\n") {
		split := strings.Index(line, " ")
		record := line[:split]
		groupsStrs := strings.Split(line[split+1:], ",")
		groups := make([]int, len(groupsStrs))
		for i, str := range groupsStrs {
			groups[i], _ = strconv.Atoi(str)
		}

		unfoldedFormat := record + "?" + record + "?" + record + "?" + record + "?" + record
		unfoldedGroups := make([]int, len(groups)*5)
		for i := 0; i < 5; i++ {
			for j, group := range groups {
				unfoldedGroups[i*len(groups)+j] = group
			}
		}

		cache := make([][]int, len(unfoldedFormat))
		for i := 0; i < len(unfoldedFormat); i++ {
			cache[i] = make([]int, len(unfoldedGroups))
		}

		total += tryArrangeCache(0, unfoldedFormat, 0, unfoldedGroups, &Cache{cache})
	}

	return total
}

type Cache struct {
	cacheArr [][]int
}

func (c *Cache) Get(si int, gi int) (int, bool) {
	val := c.cacheArr[si][gi]
	hit := val != 0
	if val == -1 {
		val = 0
	}

	return val, hit
}

func (c *Cache) Save(si int, i int, gi int) {
	if i == 0 {
		i = -1
	}

	c.cacheArr[si][gi] = i
}

func arrangementsCache(recordOffset int, record string, groupOffset int, groups []int, c *Cache) int {
	leftInGroup := groups[groupOffset]
	for i := recordOffset; i < len(record); i++ {
		if record[i] == '#' {
			leftInGroup--
		} else if record[i] == '?' {
			leftInGroup--
		} else {
			return 0
		}
		if leftInGroup == 0 {
			if i < len(record)-1 && record[i+1] == '#' {
				return 0
			} else {
				if groupOffset == len(groups)-1 {
					for j := i + 1; j < len(record); j++ {
						if record[j] == '#' {
							return 0
						}
					}
					return 1
				}
				return tryArrangeCache(i+2, record, groupOffset+1, groups, c)
			}
		}
	}

	return 0
}

func tryArrangeCache(recordOffset int, record string, groupOffset int, groups []int, c *Cache) int {
	if groupOffset >= len(groups) || recordOffset >= len(record) {
		return 0
	}

	count := 0
	for i := recordOffset; i < len(record); i++ {
		if record[i] == '#' || record[i] == '?' {
			v, hit := c.Get(i, groupOffset)
			if hit {
				count += v
			} else {
				subCount := arrangementsCache(i, record, groupOffset, groups, c)
				count += subCount
				c.Save(i, subCount, groupOffset)
			}
			if record[i] == '#' {
				return count
			}
		}
	}

	return count
}
