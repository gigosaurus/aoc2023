package day1

import (
	_ "embed"
	"strconv"
	"strings"
	"unicode"
)

//go:embed input.txt
var input string

var numberDigits = []string{"one", "two", "three", "four", "five", "six", "seven", "eight", "nine"}

func Part1() string {
	total := 0
	for _, line := range strings.Split(input, "\n") {
		lineNum, _ := strconv.Atoi(string(firstDigit(line)) + string(lastDigit(line)))
		total += lineNum
	}

	return strconv.Itoa(total)
}

func Part2() string {
	total := 0
	for _, line := range strings.Split(input, "\n") {
		first, fdi := firstDigitP2(line)
		last, ldi := lastDigitP2(line)
		firstN, fni := firstNumber(line)
		lastN, lni := lastNumber(line)

		if fni < fdi {
			first = firstN
		}

		if lni > ldi {
			last = lastN
		}

		lineNum, _ := strconv.Atoi(strconv.Itoa(first) + strconv.Itoa(last))
		total += lineNum
	}

	return strconv.Itoa(total)
}

func firstDigit(str string) rune {
	for _, char := range str {
		if unicode.IsDigit(char) {
			return char
		}
	}

	return 0
}

func lastDigit(str string) rune {
	for i := len(str) - 1; i >= 0; i-- {
		char := rune(str[i])
		if unicode.IsDigit(char) {
			return char
		}
	}

	return 0
}

func firstDigitP2(str string) (int, int) {
	for i, char := range str {
		if unicode.IsDigit(char) {
			d, _ := strconv.Atoi(string(char))
			return d, i
		}
	}

	return 0, len(str)
}

func lastDigitP2(str string) (int, int) {
	for i := len(str) - 1; i >= 0; i-- {
		char := rune(str[i])
		if unicode.IsDigit(char) {
			d, _ := strconv.Atoi(string(char))
			return d, i
		}
	}

	return 0, -1
}

func firstNumber(str string) (int, int) {
	lowest := 0
	index := len(str)
	for i, digit := range numberDigits {
		di := strings.Index(str, digit)
		if di > -1 && index > di {
			lowest = i + 1
			index = di
		}
	}

	return lowest, index
}

func lastNumber(str string) (int, int) {
	highest := 0
	index := -1
	for i, digit := range numberDigits {
		di := strings.LastIndex(str, digit)
		if di > -1 && index < di {
			highest = i + 1
			index = di
		}
	}

	return highest, index
}
