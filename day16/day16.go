package day16

import (
	_ "embed"
	"strings"
)

//go:embed input.txt
var input string

func Part1() int {
	lines := strings.Split(input, "\r\n")
	found := make([][]int, len(lines))
	for i := 0; i < len(lines); i++ {
		found[i] = make([]int, len(lines))
	}

	beam(lines, found, 0, 0, 8)

	return count(found)
}

func beam(lines []string, found [][]int, posY int, posX int, direction int) {
	for {
		if posY < 0 || posX < 0 || posY >= len(lines) || posX >= len(lines) {
			return
		}

		if found[posY][posX]&direction != 0 {
			return
		}

		found[posY][posX] |= direction

		c := lines[posY][posX]
		if c == '/' {
			if direction == 1 {
				direction = 8
			} else if direction == 2 {
				direction = 4
			} else if direction == 4 {
				direction = 2
			} else {
				direction = 1
			}
		} else if c == '\\' {
			if direction == 1 {
				direction = 4
			} else if direction == 2 {
				direction = 8
			} else if direction == 4 {
				direction = 1
			} else {
				direction = 2
			}
		} else if c == '|' {
			if direction == 4 || direction == 8 {
				beam(lines, found, posY+1, posX, 2)
				beam(lines, found, posY-1, posX, 1)
				return
			}
		} else if c == '-' {
			if direction == 1 || direction == 2 {
				beam(lines, found, posY, posX+1, 8)
				beam(lines, found, posY, posX-1, 4)
				return
			}
		}

		if direction == 1 {
			posY--
		} else if direction == 2 {
			posY++
		} else if direction == 4 {
			posX--
		} else {
			posX++
		}
	}
}

func count(found [][]int) int {
	c := 0
	for i := 0; i < len(found); i++ {
		for j := 0; j < len(found); j++ {
			if found[i][j] != 0 {
				c++
			}
		}
	}

	return c
}

func Part2() int {
	lines := strings.Split(input, "\r\n")
	highest := 0
	for y := 0; y < len(lines); y++ {
		found := make([][]int, len(lines))
		for i := 0; i < len(lines); i++ {
			found[i] = make([]int, len(lines))
		}

		beam(lines, found, y, 0, 8)
		c := count(found)
		if c > highest {
			highest = c
		}
	}

	for y := 0; y < len(lines); y++ {
		found := make([][]int, len(lines))
		for i := 0; i < len(lines); i++ {
			found[i] = make([]int, len(lines))
		}

		beam(lines, found, 0, y, 2)
		c := count(found)
		if c > highest {
			highest = c
		}
	}

	for y := 0; y < len(lines); y++ {
		found := make([][]int, len(lines))
		for i := 0; i < len(lines); i++ {
			found[i] = make([]int, len(lines))
		}

		beam(lines, found, y, len(lines)-1, 4)
		c := count(found)
		if c > highest {
			highest = c
		}
	}

	for y := 0; y < len(lines); y++ {
		found := make([][]int, len(lines))
		for i := 0; i < len(lines); i++ {
			found[i] = make([]int, len(lines))
		}

		beam(lines, found, len(lines)-1, y, 1)
		c := count(found)
		if c > highest {
			highest = c
		}
	}

	return highest
}
