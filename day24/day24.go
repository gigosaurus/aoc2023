package day24

import (
	_ "embed"
	"math"
	"strconv"
	"strings"
)

//go:embed input.txt
var input string

// example:  7, real: 200000000000000
var minB = 200000000000000

// example: 27, real: 400000000000000
var maxB = 400000000000000

type Hailstone struct {
	posX int
	posY int
	posZ int
	velX int
	velY int
	velZ int
}

func Part1() int {
	lines := strings.Split(input, "\r\n")
	hailstones := make([]*Hailstone, len(lines))
	for i, line := range lines {
		parts := strings.Split(line, " @ ")
		pos := strings.Split(parts[0], ", ")
		vel := strings.Split(parts[1], ", ")

		posX, _ := strconv.Atoi(strings.TrimSpace(pos[0]))
		posY, _ := strconv.Atoi(strings.TrimSpace(pos[1]))
		posZ, _ := strconv.Atoi(strings.TrimSpace(pos[2]))
		velX, _ := strconv.Atoi(strings.TrimSpace(vel[0]))
		velY, _ := strconv.Atoi(strings.TrimSpace(vel[1]))
		velZ, _ := strconv.Atoi(strings.TrimSpace(vel[2]))

		hailstones[i] = &Hailstone{posX, posY, posZ, velX, velY, velZ}
	}

	c := 0
	for i, h1 := range hailstones {
		for j, h2 := range hailstones {
			if j < i {
				if intersect(h1, h2) {
					c++
				}
			}
		}
	}
	return c
}

func intersect(h1 *Hailstone, h2 *Hailstone) bool {
	h1v := float64(h1.velY) / float64(h1.velX)
	h2v := float64(h2.velY) / float64(h2.velX)
	if h2v-h1v == 0 {
		return false
	}
	x := ((float64(h1.posY) - float64(h1.posX)*h1v) - (float64(h2.posY) - float64(h2.posX)*h2v)) / (h2v - h1v)
	if x < float64(minB) || x > float64(maxB) {
		return false
	}

	y := h1v*x + (float64(h1.posY) - float64(h1.posX)*h1v)
	if y < float64(minB) || y > float64(maxB) {
		return false
	}

	if (x-float64(h1.posX))*float64(h1.velX) < 0 || (x-float64(h2.posX))*float64(h2.velX) < 0 {
		return false
	}

	if (y-float64(h1.posY))*float64(h1.velY) < 0 || (y-float64(h2.posY))*float64(h2.velY) < 0 {
		return false
	}

	return true
}

func Part2() int {
	lines := strings.Split(input, "\r\n")
	hailstones := make([]*Hailstone, len(lines))
	for i, line := range lines {
		parts := strings.Split(line, " @ ")
		pos := strings.Split(parts[0], ", ")
		vel := strings.Split(parts[1], ", ")

		posX, _ := strconv.Atoi(strings.TrimSpace(pos[0]))
		posY, _ := strconv.Atoi(strings.TrimSpace(pos[1]))
		posZ, _ := strconv.Atoi(strings.TrimSpace(pos[2]))
		velX, _ := strconv.Atoi(strings.TrimSpace(vel[0]))
		velY, _ := strconv.Atoi(strings.TrimSpace(vel[1]))
		velZ, _ := strconv.Atoi(strings.TrimSpace(vel[2]))

		hailstones[i] = &Hailstone{posX, posY, posZ, velX, velY, velZ}
	}

	x, y := gaussianElimination(makeXY(hailstones))
	_, z := gaussianElimination(makeXZ(hailstones))

	return x + y + z
}

func makeXY(hailstones []*Hailstone) [][]float64 {
	m := make([][]float64, 4)
	for i := 0; i < 4; i++ {
		h1 := hailstones[i+1]
		h2 := hailstones[i]
		a := h1.velY - h2.velY
		b := h2.velX - h1.velX
		c := h1.posY - h2.posY
		d := h2.posX - h1.posX
		e := h2.posX*h2.velY - h2.posY*h2.velX - h1.posX*h1.velY + h1.posY*h1.velX
		m[i] = []float64{float64(a), float64(b), float64(c), float64(d), float64(e)}
	}

	return m
}

func makeXZ(hailstones []*Hailstone) [][]float64 {
	m := make([][]float64, 4)
	for i := 0; i < 4; i++ {
		h1 := hailstones[i+1]
		h2 := hailstones[i]
		a := h1.velZ - h2.velZ
		b := h2.velX - h1.velX
		c := h1.posZ - h2.posZ
		d := h2.posX - h1.posX
		e := h2.posX*h2.velZ - h2.posZ*h2.velX - h1.posX*h1.velZ + h1.posZ*h1.velX
		m[i] = []float64{float64(a), float64(b), float64(c), float64(d), float64(e)}
	}

	return m
}

func gaussianElimination(m [][]float64) (int, int) {
	for i := 0; i < 4; i++ {
		if m[i][i] == 0 {
			for j := i + 1; j < 4; j++ {
				if m[j][i] != 0 {
					m[i], m[j] = m[j], m[i]
					break
				}
			}
		}

		scalar := 1 / m[i][i]
		for j := range m[i] {
			m[i][j] *= scalar
		}

		for j := i + 1; j < 4; j++ {
			scalar = -m[j][i]
			for k := range m[j] {
				m[j][k] += scalar * m[i][k]
			}
		}
	}

	solution := make([]float64, 4)
	for i := 3; i >= 0; i-- {
		solution[i] = m[i][4]
		for j := i + 1; j < 4; j++ {
			solution[i] -= m[i][j] * solution[j]
		}
	}

	return int(math.Round(-solution[0])), int(math.Round(-solution[1]))
}
