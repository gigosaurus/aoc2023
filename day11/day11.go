package day11

import (
	_ "embed"
	"strings"
)

//go:embed input.txt
var input string

// example: 10, real: 140
var length = 140

func Part1() int {
	//lines := make([]string, length)
	rows := make([]bool, length)
	columns := make([]bool, length)
	var galaxies [][]int
	for y, line := range strings.Split(input, "\r\n") {
		for x, c := range line {
			if c == '#' {
				rows[y] = true
				columns[x] = true
				galaxies = append(galaxies, []int{y, x})
			}
		}
	}

	for i := length - 1; i >= 0; i-- {
		if !rows[i] {
			for _, galaxy := range galaxies {
				if galaxy[0] > i {
					galaxy[0]++
				}
			}
		}
		if !columns[i] {
			for _, galaxy := range galaxies {
				if galaxy[1] > i {
					galaxy[1]++
				}
			}
		}
	}

	total := 0
	for i, galaxy1 := range galaxies {
		for j := i + 1; j < len(galaxies); j++ {
			galaxy2 := galaxies[j]
			if galaxy2[1] > galaxy1[1] {
				total += galaxy2[0] - galaxy1[0] + galaxy2[1] - galaxy1[1]
			} else {
				total += galaxy2[0] - galaxy1[0] + galaxy1[1] - galaxy2[1]
			}
		}
	}

	return total
}

func Part2() int {
	//lines := make([]string, length)
	rows := make([]bool, length)
	columns := make([]bool, length)
	var galaxies [][]int
	for y, line := range strings.Split(input, "\r\n") {
		for x, c := range line {
			if c == '#' {
				rows[y] = true
				columns[x] = true
				galaxies = append(galaxies, []int{y, x})
			}
		}
	}

	for i := length - 1; i >= 0; i-- {
		if !rows[i] {
			for _, galaxy := range galaxies {
				if galaxy[0] > i {
					galaxy[0] += 999999
				}
			}
		}
		if !columns[i] {
			for _, galaxy := range galaxies {
				if galaxy[1] > i {
					galaxy[1] += 999999
				}
			}
		}
	}

	total := 0
	for i, galaxy1 := range galaxies {
		for j := i + 1; j < len(galaxies); j++ {
			galaxy2 := galaxies[j]
			if galaxy2[1] > galaxy1[1] {
				total += galaxy2[0] - galaxy1[0] + galaxy2[1] - galaxy1[1]
			} else {
				total += galaxy2[0] - galaxy1[0] + galaxy1[1] - galaxy2[1]
			}
		}
	}

	return total
}
