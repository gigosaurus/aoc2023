package day18

import (
	_ "embed"
	"strconv"
	"strings"
)

//go:embed input.txt
var input string

func Part1() int {
	var points []*Point
	x := 0
	y := 0
	minX := 0
	maxX := 0
	minY := 0
	maxY := 0
	totalDist := 0
	for _, line := range strings.Split(input, "\r\n") {
		parts := strings.Split(line, " ")
		dir := line[0]
		dist, _ := strconv.Atoi(parts[1])
		points = append(points, &Point{x, y})
		if dir == 'R' {
			x += dist
			if x > maxX {
				maxX = x
			}
		} else if dir == 'L' {
			x -= dist
			if x < minX {
				minX = x
			}
		} else if dir == 'U' {
			y -= dist
			if y < minY {
				minY = y
			}
		} else if dir == 'D' {
			y += dist
			if y > maxY {
				maxY = y
			}
		}
		totalDist += dist
	}

	points = append(points, &Point{0, 0})

	sum := 0
	reverseSum := 0
	lastPoint := points[0]
	for i, point := range points {
		point.x -= minX
		point.y -= minY
		if i > 0 {
			sum += point.x * lastPoint.y
			reverseSum += lastPoint.x * point.y
		}
		lastPoint = point
	}

	if sum > reverseSum {
		sum = (sum - reverseSum) / 2
	} else {
		sum = (reverseSum - sum) / 2
	}

	return sum + (totalDist / 2) + 1
}

type Point struct {
	x int
	y int
}

func Part2() int {
	var points []*Point
	x := 0
	y := 0
	minX := 0
	maxX := 0
	minY := 0
	maxY := 0
	totalDist := 0
	for _, line := range strings.Split(input, "\r\n") {
		parts := strings.Split(line, " ")
		dir := line[len(line)-2]
		hex := parts[2][2:7]
		dist, _ := strconv.ParseUint(hex, 16, 32)
		points = append(points, &Point{x, y})
		if dir == '0' {
			x += int(dist)
			if x > maxX {
				maxX = x
			}
		} else if dir == '2' {
			x -= int(dist)
			if x < minX {
				minX = x
			}
		} else if dir == '3' {
			y -= int(dist)
			if y < minY {
				minY = y
			}
		} else if dir == '1' {
			y += int(dist)
			if y > maxY {
				maxY = y
			}
		}
		totalDist += int(dist)
	}

	points = append(points, &Point{0, 0})

	sum := 0
	reverseSum := 0
	lastPoint := points[0]
	for i, point := range points {
		point.x -= minX
		point.y -= minY
		if i > 0 {
			sum += point.x * lastPoint.y
			reverseSum += lastPoint.x * point.y
		}
		lastPoint = point
	}

	if sum > reverseSum {
		sum = (sum - reverseSum) / 2
	} else {
		sum = (reverseSum - sum) / 2
	}

	return sum + (totalDist / 2) + 1
}
