package day22

import (
	"cmp"
	_ "embed"
	"slices"
	"strconv"
	"strings"
)

//go:embed input.txt
var input string

// example: 3, real: 10
var length = 10

type Point struct {
	x int
	y int
}

type Block struct {
	id       int
	from     Point
	to       Point
	z        int
	height   int
	supports []*Block
	depends  []*Block
}

func (b *Block) IsSupported() bool {
	return len(b.depends) > 0
}

func (b *Block) AddSupport(block *Block) {
	b.supports = append(b.supports, block)
	block.depends = append(block.depends, b)
}

func (b *Block) HasChecked(block *Block) bool {
	for i := 0; i < len(b.supports); i++ {
		if b.supports[i].id == block.id {
			return true
		}
	}

	for i := 0; i < len(b.depends); i++ {
		if b.depends[i].id == block.id {
			return true
		}
	}

	return false
}

func (b *Block) CheckSupports(block *Block) {
	if b.HasChecked(block) || block.id == b.id {
		return
	}
	if b.z+b.height != block.z {
		return
	}
	for cbX := block.from.x; cbX <= block.to.x; cbX++ {
		for cbY := block.from.y; cbY <= block.to.y; cbY++ {
			for nbX := b.from.x; nbX <= b.to.x; nbX++ {
				for nbY := b.from.y; nbY <= b.to.y; nbY++ {
					if cbX == nbX && cbY == nbY {
						b.AddSupport(block)
						return
					}
				}
			}
		}
	}
}

func (b *Block) CanDisintegrate() bool {
	for _, block := range b.supports {
		if len(block.depends) == 1 {
			return false
		}
	}

	return true
}

func (b *Block) Disintegrate() int {
	if b.CanDisintegrate() {
		return 0
	}
	fallen := make(map[int]bool)
	supports := b.supports
	for {
		if len(supports) == 0 {
			return len(fallen)
		}

		newBlocks := make([]*Block, 0)
		for i := 0; i < len(supports); i++ {
			if !fallen[supports[i].id] {
				willFall := true
				for j := 0; j < len(supports[i].depends); j++ {
					if supports[i].depends[j].id != b.id && !fallen[supports[i].depends[j].id] {
						willFall = false
						break
					}
				}
				if willFall {
					newBlocks = append(newBlocks, supports[i].supports...)
					fallen[supports[i].id] = true
				}
			}
		}

		supports = newBlocks
	}
}

func fall(blocks []*Block) int {
	lowest := make([][]int, length)
	for i := 0; i < length; i++ {
		lowest[i] = make([]int, length)
	}

	maxZ := 0
	for _, block := range blocks {
		pos := 0
		for x := block.from.x; x <= block.to.x; x++ {
			for y := block.from.y; y <= block.to.y; y++ {
				if lowest[y][x] > pos {
					pos = lowest[y][x]
				}
			}
		}
		for x := block.from.x; x <= block.to.x; x++ {
			for y := block.from.y; y <= block.to.y; y++ {
				lowest[y][x] = pos + block.height
			}
		}
		block.z = pos + 1
		if maxZ < block.z+block.height {
			maxZ = block.z + block.height
		}
	}

	return maxZ
}

func readInput() []*Block {
	blocks := make([]*Block, 0)
	for i, line := range strings.Split(input, "\r\n") {
		split := strings.Split(line, "~")
		from := strings.Split(split[0], ",")
		to := strings.Split(split[1], ",")
		fromX, _ := strconv.Atoi(from[0])
		fromY, _ := strconv.Atoi(from[1])
		fromZ, _ := strconv.Atoi(from[2])
		toX, _ := strconv.Atoi(to[0])
		toY, _ := strconv.Atoi(to[1])
		toZ, _ := strconv.Atoi(to[2])
		blocks = append(blocks, &Block{i, Point{fromX, fromY}, Point{toX, toY}, fromZ, 1 + toZ - fromZ, make([]*Block, 0), make([]*Block, 0)})
	}

	return blocks
}

func Part1() int {
	blocks := readInput()

	slices.SortFunc(blocks, func(i *Block, j *Block) int {
		return cmp.Compare(i.z, j.z)
	})

	maxZ := fall(blocks)

	blocksByZ := make([][]*Block, maxZ+1)
	for _, block := range blocks {
		for z := block.z; z < block.z+block.height; z++ {
			blocksByZ[z-1] = append(blocksByZ[z-1], block)
		}
	}

	for z := 0; z < maxZ; z++ {
		currentLevel := blocksByZ[z]
		nextLevel := blocksByZ[z+1]
		for _, block := range currentLevel {
			for _, nextBlock := range nextLevel {
				block.CheckSupports(nextBlock)
			}
		}
	}

	d := 0
	for _, block := range blocks {
		if block.CanDisintegrate() {
			d++
		}
	}
	return d
}

func Part2() int {
	blocks := readInput()

	slices.SortFunc(blocks, func(i *Block, j *Block) int {
		return cmp.Compare(i.z, j.z)
	})

	maxZ := fall(blocks)

	blocksByZ := make([][]*Block, maxZ+1)
	for _, block := range blocks {
		for z := block.z; z < block.z+block.height; z++ {
			blocksByZ[z-1] = append(blocksByZ[z-1], block)
		}
	}

	for z := 0; z < maxZ; z++ {
		currentLevel := blocksByZ[z]
		nextLevel := blocksByZ[z+1]
		for _, block := range currentLevel {
			for _, nextBlock := range nextLevel {
				block.CheckSupports(nextBlock)
			}
		}
	}

	s := 0
	for _, block := range blocks {
		s += block.Disintegrate()
	}
	return s
}
