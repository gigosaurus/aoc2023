package utils

import (
	"fmt"
	"time"
)

func Timing(name string, f func()) int64 {
	iterations := 1
	duration := int64(0)
	for {
		start := time.Now()
		for i := iterations; i > 0; i-- {
			f()
		}

		since := time.Since(start)
		if since > 50*time.Millisecond {
			duration = since.Nanoseconds() / int64(iterations)
			break
		}

		iterations *= 2
	}

	PrintFormattedMicroSeconds(name, duration)

	return duration
}

func PrintFormattedMicroSeconds(name string, duration int64) {
	if duration < int64(time.Microsecond) {
		fmt.Printf("%.3fµs\t\t%s\n", float64(duration)/float64(time.Microsecond), name)
	} else if duration < int64(10*time.Microsecond) {
		fmt.Printf("%.2fµs\t\t%s\n", float64(duration)/float64(time.Microsecond), name)
	} else if duration < int64(100*time.Microsecond) {
		fmt.Printf("%.1fµs\t\t%s\n", float64(duration)/float64(time.Microsecond), name)
	} else {
		fmt.Printf("%dµs\t\t%s\n", duration/int64(time.Microsecond), name)
	}
}
func PrintFormattedMilliSeconds(name string, duration int64) {
	if duration < int64(time.Millisecond) {
		fmt.Printf("%.3fms\t\t%s\n", float64(duration)/float64(time.Millisecond), name)
	} else if duration < int64(10*time.Millisecond) {
		fmt.Printf("%.2fms\t\t%s\n", float64(duration)/float64(time.Millisecond), name)
	} else if duration < int64(100*time.Millisecond) {
		fmt.Printf("%.1fms\t\t%s\n", float64(duration)/float64(time.Millisecond), name)
	} else {
		fmt.Printf("%dms\t\t%s\n", duration/int64(time.Millisecond), name)
	}
}

func Min(a int, b int) int {
	if a < b {
		return a
	}

	return b
}

func Max(a int, b int) int {
	if a > b {
		return a
	}

	return b
}
