package day14

import (
	_ "embed"
	"strings"
)

//go:embed input.txt
var input string

// example 10, real 100
var length = 100

func Part1() int {
	roundedRocks := make([][]bool, length)
	cubeRocks := make([][]bool, length)
	for y, line := range strings.Split(input, "\r\n") {
		roundedRocks[y] = make([]bool, length)
		cubeRocks[y] = make([]bool, length)
		for x, c := range line {
			if c == 'O' {
				roundedRocks[y][x] = true
			} else if c == '#' {
				cubeRocks[y][x] = true
			}
		}
	}

	return calculateLoad(roundedRocks, cubeRocks)
}

func calculateLoad(roundedRocks [][]bool, cubeRocks [][]bool) int {
	total := 0
	for x := 0; x < length; x++ {
		load := length
		for y := 0; y < length; y++ {
			if roundedRocks[y][x] {
				total += load
				load--
			} else if cubeRocks[y][x] {
				load = length - y - 1
			}
		}
	}

	return total
}

func cycle(roundedRocks [][]bool, cubeRocks [][]bool) {
	// north
	for x := 0; x < length; x++ {
		newY := 0
		for y := 0; y < length; y++ {
			if roundedRocks[y][x] {
				if newY != y {
					roundedRocks[y][x] = false
					roundedRocks[newY][x] = true
				}
				newY++
			} else if cubeRocks[y][x] {
				newY = y + 1
			}
		}
	}

	// west
	for y := 0; y < length; y++ {
		newX := 0
		for x := 0; x < length; x++ {
			if roundedRocks[y][x] {
				if newX != x {
					roundedRocks[y][x] = false
					roundedRocks[y][newX] = true
				}
				newX++
			} else if cubeRocks[y][x] {
				newX = x + 1
			}
		}
	}

	// south
	for x := 0; x < length; x++ {
		newY := length - 1
		for y := length - 1; y >= 0; y-- {
			if roundedRocks[y][x] {
				if newY != y {
					roundedRocks[y][x] = false
					roundedRocks[newY][x] = true
				}
				newY--
			} else if cubeRocks[y][x] {
				newY = y - 1
			}
		}
	}

	// west
	for y := 0; y < length; y++ {
		newX := length - 1
		for x := length - 1; x >= 0; x-- {
			if roundedRocks[y][x] {
				if newX != x {
					roundedRocks[y][x] = false
					roundedRocks[y][newX] = true
				}
				newX--
			} else if cubeRocks[y][x] {
				newX = x - 1
			}
		}
	}
}

func hash(roundedRocks [][]bool) int {
	check := 0
	for x := 0; x < length; x++ {
		for y := 0; y < length; y++ {
			if roundedRocks[y][x] {
				check += (x + 1) * (y + 1)
			}
		}
	}

	return check
}

func Part2() int {
	roundedRocks := make([][]bool, length)
	cubeRocks := make([][]bool, length)
	for y, line := range strings.Split(input, "\r\n") {
		roundedRocks[y] = make([]bool, length)
		cubeRocks[y] = make([]bool, length)
		for x, c := range line {
			if c == 'O' {
				roundedRocks[y][x] = true
			} else if c == '#' {
				cubeRocks[y][x] = true
			}
		}
	}

	hashes := make(map[int]int)
	found := false
	for i := 0; i < 1000000000; i++ {
		cycle(roundedRocks, cubeRocks)
		if !found {
			h := hash(roundedRocks)
			if j, ok := hashes[h]; ok {
				i = 1000000000 - (1000000000-i)%(i-j)
				found = true
			} else {
				hashes[h] = i
			}
		}
	}

	total := 0
	for x := 0; x < length; x++ {
		for y := 0; y < length; y++ {
			if roundedRocks[y][x] {
				total += length - y
			}
		}
	}

	return total
}
