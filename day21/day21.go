package day21

import (
	_ "embed"
	"strconv"
	"strings"
)

//go:embed input.txt
var input string
var length = 131

type Point struct {
	x int
	y int
}

type Points struct {
	points map[string]bool
}

func (p *Points) Add(points []Point) {
	for _, point := range points {
		p.add(point)
	}
}

func (p *Points) add(point Point) {
	p.points[strconv.Itoa(point.x)+","+strconv.Itoa(point.y)] = true
}

func (p *Points) Get() []Point {
	points := make([]Point, 0)
	for k := range p.points {
		parts := strings.Split(k, ",")
		x, _ := strconv.Atoi(parts[0])
		y, _ := strconv.Atoi(parts[1])
		points = append(points, Point{x, y})
	}
	return points
}

func Part1() int {
	p, g := readGarden()
	return WalkFrom(p, 64, g)
}

func readGarden() (Point, []bool) {
	garden := make([]bool, length*length)
	start := Point{}
	for y, line := range strings.Split(input, "\r\n") {
		for x, c := range line {
			if c == '#' {
				garden[y*length+x] = false
			} else if c == 'S' {
				garden[y*length+x] = true
				start = Point{x, y}
			} else {
				garden[y*length+x] = true
			}
		}
	}

	return start, garden
}

func WalkFrom(p Point, stepsC int, g []bool) int {
	steps := make([]Point, 1)
	steps[0] = p
	c := 0
	for i := 0; i < stepsC; i++ {
		newSteps := &Points{map[string]bool{}}
		for _, point := range steps {
			newSteps.Add(step(point, g))
		}
		steps = newSteps.Get()
		c = len(steps)
	}
	return c
}

func step(p Point, g []bool) []Point {
	var points []Point
	gX := p.x % length
	gY := p.y % length
	if gX < 0 {
		gX += length
	}
	if gY < 0 {
		gY += length
	}
	if gX == 0 {
		if g[gY*length+length-1] {
			points = append(points, Point{p.x - 1, p.y})
		}
	} else if g[gY*length+gX-1] {
		points = append(points, Point{p.x - 1, p.y})
	}
	if gY == 0 {
		if g[(length-1)*length+gX] {
			points = append(points, Point{p.x, p.y - 1})
		}
	} else if g[(gY-1)*length+gX] {
		points = append(points, Point{p.x, p.y - 1})
	}
	if gX == length-1 {
		if g[gY*length] {
			points = append(points, Point{p.x + 1, p.y})
		}
	} else if g[gY*length+gX+1] {
		points = append(points, Point{p.x + 1, p.y})
	}
	if gY == length-1 {
		if g[gX] {
			points = append(points, Point{p.x, p.y + 1})
		}
	} else if g[(gY+1)*length+gX] {
		points = append(points, Point{p.x, p.y + 1})
	}

	return points
}

func Part2() int {
	steps := 26501365
	remainder := steps % length
	s, g := readGarden()
	v1 := WalkFrom(s, remainder, g)
	v2 := WalkFrom(s, remainder+length, g)
	v3 := WalkFrom(s, remainder+2*length, g)

	a := (v1 - 2*v2 + v3) / 2
	b := (-3*v1 + 4*v2 - v3) / 2
	c := v1
	x := steps / length

	return a*x*x + b*x + c
}
