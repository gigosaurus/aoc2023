package day10

import (
	_ "embed"
	"strings"
)

//go:embed input.txt
var input string

// example 10, real 140
var size = 140

func Part1() int {
	lines := strings.Split(input, "\r\n")
	startX, startY := findStart(lines)
	dist := 0
	x := startX
	y := startY
	xFrom := startX
	yFrom := startY
	for {
		newX, newY := findConnection(lines, x, y, xFrom, yFrom)
		dist++
		if newX == startX && newY == startY {
			return dist / 2
		}
		xFrom = x
		yFrom = y
		x = newX
		y = newY
	}
}

func findStart(lines []string) (int, int) {
	for y, line := range lines {
		start := strings.IndexByte(line, 'S')
		if start != -1 {
			return start, y
		}
	}

	return -1, -1
}

func findConnection(lines []string, currentX int, currentY int, fromX int, fromY int) (int, int) {
	tile := lines[currentY][currentX]
	check1X := currentX
	check2X := currentX
	check1Y := currentY
	check2Y := currentY
	if tile == '|' {
		check1Y--
		check2Y++
	} else if tile == '-' {
		check1X--
		check2X++
	} else if tile == 'L' {
		check1Y--
		check2X++
	} else if tile == 'J' {
		check1Y--
		check2X--
	} else if tile == '7' {
		check1Y++
		check2X--
	} else if tile == 'F' {
		check1Y++
		check2X++
	} else if tile == 'S' {
		if currentY > 0 {
			above := lines[currentY-1][currentX]
			if above == '|' || above == '7' || above == 'F' {
				return currentX, currentY - 1
			}
		}
		if currentX > 0 {
			left := lines[currentY][currentX-1]
			if left == '-' || left == 'L' || left == 'F' {
				return currentX - 1, currentY
			}
		}
		if currentY < size-1 {
			below := lines[currentY+1][currentX]
			if below == '|' || below == 'L' || below == 'J' {
				return currentX, currentY + 1
			}
		}
		if currentX < size-1 {
			right := lines[currentY][currentX+1]
			if right == '-' || right == 'J' || right == '7' {
				return currentX + 1, currentY
			}
		}
	}

	if !(check1X == fromX && check1Y == fromY) {
		return check1X, check1Y
	}

	return check2X, check2Y
}

func Part2() int {
	lines := strings.Split(input, "\r\n")
	tiles := make([][]byte, size)
	for i, line := range lines {
		tiles[i] = []byte(line)
	}
	startX, startY := findStart(lines)
	tiles[startY][startX] = '#'
	x := startX
	y := startY
	xFrom := startX
	yFrom := startY
	for {
		newX, newY := findConnection(lines, x, y, xFrom, yFrom)
		tiles[newY][newX] = '#'
		if newX == startX && newY == startY {
			break
		}
		xFrom = x
		yFrom = y
		x = newX
		y = newY
	}

	inLoop := 0
	for rY, row := range tiles {
		inside := false
		last := '.'
		for rX, tile := range row {
			if tile == '#' {
				actualTile := lines[rY][rX]
				if actualTile == '|' {
					inside = !inside
					last = '|'
				} else if actualTile == 'L' {
					inside = !inside
					last = 'L'
				} else if actualTile == 'J' {
					if last != 'F' {
						inside = !inside
					}
					last = 'J'
				} else if actualTile == '7' {
					if last != 'L' {
						inside = !inside
					}
					last = '7'
				} else if actualTile == 'F' {
					inside = !inside
					last = 'F'
				} else if actualTile == 'S' {
					inside = !inside
				}
			} else if inside {
				inLoop++
			}
		}
	}

	return inLoop
}
