package day2

import (
	_ "embed"
	"strconv"
	"strings"
)

//go:embed input.txt
var input string

var maxColours = map[string]int{
	"red":   12,
	"green": 13,
	"blue":  14,
}

func Part1() int {
	total := 0
	for _, line := range strings.Split(input, "\n") {
		total += checkLine(line + ",")
	}

	return total
}

func checkLine(line string) int {
	num := ""
	token := ""
	gameNum := 0
	for _, char := range line {
		if char >= 48 && char <= 57 {
			if num != "" {
				num += string(char)
			} else {
				num = string(char)
			}
		} else if char >= 97 && char <= 122 {
			token += string(char)
		} else if char == 58 {
			gameNum, _ = strconv.Atoi(num)
			num = ""
			token = ""
		} else if char == 44 || char == 59 {
			maxColor, ok := maxColours[token]
			if ok {
				number, _ := strconv.Atoi(num)
				if number > maxColor {
					return 0
				}
			}
			num = ""
			token = ""
		}
	}

	return gameNum
}

func Part2() int {
	total := 0
	for _, line := range strings.Split(input, "\n") {
		total += checkLineP2(line + ",")
	}

	return total
}

func checkLineP2(line string) int {
	num := ""
	token := ""
	highest := map[string]int{
		"red":   0,
		"green": 0,
		"blue":  0,
	}
	for _, char := range line {
		if char >= 48 && char <= 57 {
			if num != "" {
				num += string(char)
			} else {
				num = string(char)
			}
		} else if char >= 97 && char <= 122 {
			token += string(char)
		} else if char == 58 {
			num = ""
			token = ""
		} else if char == 44 || char == 59 {
			highestColour, ok := highest[token]
			if ok {
				number, _ := strconv.Atoi(num)
				if number > highestColour {
					highest[token] = number
				}
			}
			num = ""
			token = ""
		}
	}

	return highest["red"] * highest["green"] * highest["blue"]
}
