package main

import (
	"aoc2023/day1"
	"aoc2023/day10"
	"aoc2023/day11"
	"aoc2023/day12"
	"aoc2023/day13"
	"aoc2023/day14"
	"aoc2023/day15"
	"aoc2023/day16"
	"aoc2023/day17"
	"aoc2023/day18"
	"aoc2023/day19"
	"aoc2023/day2"
	"aoc2023/day20"
	"aoc2023/day21"
	"aoc2023/day22"
	"aoc2023/day23"
	"aoc2023/day24"
	"aoc2023/day3"
	"aoc2023/day4"
	"aoc2023/day5"
	"aoc2023/day6"
	"aoc2023/day7"
	"aoc2023/day8"
	"aoc2023/day9"
	"aoc2023/utils"
	"fmt"
)

func main() {
	//fmt.Println(day24.Part1())
	//fmt.Println(day24.Part2())
	timings()
}

func timings() {
	total := int64(0)
	total += utils.Timing("Trebuchet", func() { day1.Part1() })
	total += utils.Timing("", func() { day1.Part2() })
	total += utils.Timing("Cube Conundrum", func() { day2.Part1() })
	total += utils.Timing("", func() { day2.Part2() })
	total += utils.Timing("Gear Ratios", func() { day3.Part1() })
	total += utils.Timing("", func() { day3.Part2() })
	total += utils.Timing("Scratchcards", func() { day4.Part1() })
	total += utils.Timing("", func() { day4.Part2() })
	total += utils.Timing("If You Give A Seed A Fertilizer", func() { day5.Part1() })
	total += utils.Timing("", func() { day5.Part2() })
	total += utils.Timing("Wait For It", func() { day6.Part1() })
	total += utils.Timing("", func() { day6.Part2() })
	total += utils.Timing("Camel Cards", func() { day7.Part1() })
	total += utils.Timing("", func() { day7.Part2() })
	total += utils.Timing("Haunted Wasteland", func() { day8.Part1() })
	total += utils.Timing("", func() { day8.Part2() })
	total += utils.Timing("Mirage Maintenance", func() { day9.Part1() })
	total += utils.Timing("", func() { day9.Part2() })
	total += utils.Timing("Pipe Maze", func() { day10.Part1() })
	total += utils.Timing("", func() { day10.Part2() })
	total += utils.Timing("Cosmic Expansion", func() { day11.Part1() })
	total += utils.Timing("", func() { day11.Part2() })
	total += utils.Timing("Hot Springs", func() { day12.Part1() })
	total += utils.Timing("", func() { day12.Part2() })
	total += utils.Timing("Point of Incidence", func() { day13.Part1() })
	total += utils.Timing("", func() { day13.Part2() })
	total += utils.Timing("Parabolic Reflector Dish", func() { day14.Part1() })
	total += utils.Timing("", func() { day14.Part2() })
	total += utils.Timing("Lens Library", func() { day15.Part1() })
	total += utils.Timing("", func() { day15.Part2() })
	total += utils.Timing("The Floor Will Be Lava", func() { day16.Part1() })
	total += utils.Timing("", func() { day16.Part2() })
	total += utils.Timing("Clumsy Crucible", func() { day17.Part1() })
	total += utils.Timing("", func() { day17.Part2() })
	total += utils.Timing("Lavaduct Lagoon", func() { day18.Part1() })
	total += utils.Timing("", func() { day18.Part2() })
	total += utils.Timing("Aplenty", func() { day19.Part1() })
	total += utils.Timing("", func() { day19.Part2() })
	total += utils.Timing("Pulse Propagation", func() { day20.Part1() })
	total += utils.Timing("", func() { day20.Part2() })
	total += utils.Timing("Step Counter", func() { day21.Part1() })
	total += utils.Timing("", func() { day21.Part2() })
	total += utils.Timing("Sand Slabs", func() { day22.Part1() })
	total += utils.Timing("", func() { day22.Part2() })
	total += utils.Timing("A Long Walk", func() { day23.Part1() })
	total += utils.Timing("", func() { day23.Part2() })
	total += utils.Timing("Never Tell Me The Odds", func() { day24.Part1() })
	total += utils.Timing("", func() { day24.Part2() })

	fmt.Println("----------------")
	utils.PrintFormattedMilliSeconds("", total)
}
