package day13

import (
	"aoc2023/utils"
	_ "embed"
	"strings"
)

//go:embed input.txt
var input string

func Part1() int {
	total := 0
	for _, patternString := range strings.Split(input, "\r\n\r\n") {
		patternLines := strings.Split(patternString, "\r\n")
		row := make([]int, len(patternLines))
		col := make([]int, len(patternLines[0]))
		for x, line := range patternLines {
			for y, c := range line {
				if c == '#' {
					row[x] |= 1 << y
					col[y] |= 1 << x
				}
			}
		}

		i, ok := findReflection(row)
		if ok {
			total += 100 * i
		} else {
			i, ok = findReflection(col)
			if ok {
				total += i
			}
		}
	}

	return total
}

func findReflection(data []int) (int, bool) {
	for i := 1; i < len(data); i++ {
		jMax := utils.Min(i, len(data)-i)
		same := true
		for j := 0; j < jMax; j++ {
			if data[i-j-1] != data[i+j] {
				same = false
				break
			}
		}
		if same {
			return i, true
		}
	}

	return 0, false
}

func Part2() int {
	total := 0
	for _, patternString := range strings.Split(input, "\r\n\r\n") {
		patternLines := strings.Split(patternString, "\r\n")
		row := make([]int, len(patternLines))
		col := make([]int, len(patternLines[0]))
		for x, line := range patternLines {
			for y, c := range line {
				if c == '#' {
					row[x] |= 1 << y
					col[y] |= 1 << x
				}
			}
		}

		i, ok := findSmudgedReflection(row)
		if ok {
			total += i * 100
		} else {
			i, ok = findSmudgedReflection(col)
			if ok {
				total += i
			}
		}
	}

	return total
}

func findSmudgedReflection(data []int) (int, bool) {
	for i := 1; i < len(data); i++ {
		jMax := utils.Min(i, len(data)-i)
		diffByOne := false
		for j := 0; j < jMax; j++ {
			if data[i-j-1] != data[i+j] {
				diff := data[i-j-1] ^ data[i+j]
				if diff&(diff-1) == 0 {
					if diffByOne {
						diffByOne = false
						break
					}
					diffByOne = true
				} else {
					diffByOne = false
					break
				}
			}
		}
		if diffByOne {
			return i, true
		}
	}

	return 0, false
}
