package day15

import (
	_ "embed"
	"strconv"
	"strings"
)

//go:embed input.txt
var input string

func Part1() int {
	total := 0
	for _, step := range strings.Split(input, ",") {
		total += hash(step)
	}

	return total
}

func hash(str string) int {
	value := 0
	for _, c := range str {
		value += int(c)
		value *= 17
		value &= 0xff
	}

	return value
}

type lense struct {
	label  int
	length int
}

type Box struct {
	lenses []*lense
}

func (b *Box) hashLabel(label string) int {
	value := 0
	for _, c := range label {
		value <<= 5
		value |= int(c - 'a')
	}

	return value
}

func (b *Box) add(label string, length int) {
	h := b.hashLabel(label)
	for _, l := range b.lenses {
		if l.label == h {
			l.length = length
			return
		}
	}

	b.lenses = append(b.lenses, &lense{label: h, length: length})
}

func (b *Box) remove(label string) {
	h := b.hashLabel(label)
	for i, l := range b.lenses {
		if l.label == h {
			b.lenses = append(b.lenses[:i], b.lenses[i+1:]...)
			return
		}
	}
}

func (b *Box) power() int {
	power := 0
	for i, l := range b.lenses {
		power += (i + 1) * l.length
	}

	return power
}

func Part2() int {
	total := 0
	boxes := make([]*Box, 256)
	for _, step := range strings.Split(input, ",") {
		focal := strings.IndexByte(step, '=')
		if focal == -1 {
			label := step[:len(step)-1]
			box := hash(label)
			if boxes[box] == nil {
				boxes[box] = &Box{make([]*lense, 0)}
			}
			boxes[box].remove(label)
		} else {
			label := step[:focal]
			box := hash(label)
			length, _ := strconv.Atoi(step[focal+1:])
			if boxes[box] == nil {
				boxes[box] = &Box{make([]*lense, 0)}
			}
			boxes[box].add(label, length)
		}
	}

	for i, box := range boxes {
		if box != nil {
			total += (i + 1) * box.power()
		}
	}

	return total
}
