package day19

import (
	_ "embed"
	"strconv"
	"strings"
)

//go:embed input.txt
var input string

type Workflow struct {
	name  string
	rules []*Rule
}

type Rule struct {
	part     uint8
	lessThan bool
	value    int
	result   string
}

func Part1() int {
	workflows := make(map[string]*Workflow)
	sorting := false
	total := 0
	for _, line := range strings.Split(input, "\r\n") {
		if len(line) == 0 {
			sorting = true
			continue
		}

		if !sorting {
			ruleStart := strings.IndexByte(line, '{')
			ruleEnd := strings.LastIndexByte(line, '}')
			rules := strings.Split(line[ruleStart+1:ruleEnd], ",")
			workflowRules := make([]*Rule, len(rules))
			for i, rule := range rules {
				parts := strings.Split(rule, ":")
				if len(parts) != 2 {
					workflowRules[i] = &Rule{'n', false, 0, parts[0]}
				} else {
					num, _ := strconv.Atoi(parts[0][2:])
					workflowRules[i] = &Rule{parts[0][0], parts[0][1] == '<', num, parts[1]}
				}
			}
			workflows[line[:ruleStart]] = &Workflow{line[:ruleStart-1], workflowRules}
		} else {
			parts := strings.Split(line[1:len(line)-1], ",")
			// always x, m, a, s
			x, _ := strconv.Atoi(parts[0][2:])
			m, _ := strconv.Atoi(parts[1][2:])
			a, _ := strconv.Atoi(parts[2][2:])
			s, _ := strconv.Atoi(parts[3][2:])

			workflowName := "in"
			result := ""
			for {
				if result != "" {
					if result == "A" || result == "R" {
						break
					}
					workflowName = result
					result = ""
				}
				workflow := workflows[workflowName]
				for _, rule := range workflow.rules {
					if result != "" {
						break
					}
					if rule.part == 'x' {
						result = check(rule, x)
					} else if rule.part == 'm' {
						result = check(rule, m)
					} else if rule.part == 'a' {
						result = check(rule, a)
					} else if rule.part == 's' {
						result = check(rule, s)
					} else {
						result = rule.result
					}
				}
			}

			if result == "A" {
				total += x + m + a + s
			}

		}
	}
	return total
}

func check(rule *Rule, v int) string {
	if rule.lessThan && v < rule.value || !rule.lessThan && v > rule.value {
		return rule.result
	}

	return ""
}

func Part2() int {
	workflows := make(map[string]*Workflow)
	for _, line := range strings.Split(input, "\r\n") {
		if len(line) == 0 {
			break
		}

		ruleStart := strings.IndexByte(line, '{')
		ruleEnd := strings.LastIndexByte(line, '}')
		rules := strings.Split(line[ruleStart+1:ruleEnd], ",")
		workflowRules := make([]*Rule, len(rules))
		for i, rule := range rules {
			parts := strings.Split(rule, ":")
			if len(parts) != 2 {
				workflowRules[i] = &Rule{'n', false, 0, parts[0]}
			} else {
				num, _ := strconv.Atoi(parts[0][2:])
				workflowRules[i] = &Rule{parts[0][0], parts[0][1] == '<', num, parts[1]}
			}
		}
		workflows[line[:ruleStart]] = &Workflow{line[:ruleStart-1], workflowRules}
	}

	return routes([]int{1, 1, 1, 1}, []int{4000, 4000, 4000, 4000}, "in", workflows, []string{})
}

func routes(min []int, max []int, name string, workflows map[string]*Workflow, path []string) int {
	path = append(path, name)
	if name == "R" {
		return 0
	}

	if name == "A" {
		t := 1
		for i := 0; i < 4; i++ {
			t *= max[i] - min[i] + 1
		}
		return t
	}

	result := 0
	for _, rule := range workflows[name].rules {
		if rule.part == 'x' {
			if rule.lessThan {
				if max[0] < rule.value {
					result += routes(min, max, rule.result, workflows, path)
					return result
				}
				if min[0] < rule.value {
					result += routes(min, []int{rule.value - 1, max[1], max[2], max[3]}, rule.result, workflows, path)
					min = []int{rule.value, min[1], min[2], min[3]}
				}
			} else {
				if min[0] > rule.value {
					result += routes(min, max, rule.result, workflows, path)
					return result
				}
				if max[0] > rule.value {
					result += routes([]int{rule.value + 1, min[1], min[2], min[3]}, max, rule.result, workflows, path)
					max = []int{rule.value, max[1], max[2], max[3]}
				}
			}
		} else if rule.part == 'm' {
			if rule.lessThan {
				if max[1] < rule.value {
					result += routes(min, max, rule.result, workflows, path)
					return result
				}
				if min[1] < rule.value {
					result += routes(min, []int{max[0], rule.value - 1, max[2], max[3]}, rule.result, workflows, path)
					min = []int{min[0], rule.value, min[2], min[3]}
				}
			} else {
				if min[1] > rule.value {
					result += routes(min, max, rule.result, workflows, path)
					return result
				}
				if max[1] > rule.value {
					result += routes([]int{min[0], rule.value + 1, min[2], min[3]}, max, rule.result, workflows, path)
					max = []int{max[0], rule.value, max[2], max[3]}
				}
			}
		} else if rule.part == 'a' {
			if rule.lessThan {
				if max[2] < rule.value {
					result += routes(min, max, rule.result, workflows, path)
					return result
				}
				if min[2] < rule.value {
					result += routes(min, []int{max[0], max[1], rule.value - 1, max[3]}, rule.result, workflows, path)
					min = []int{min[0], min[1], rule.value, min[3]}
				}
			} else {
				if min[2] > rule.value {
					result += routes(min, max, rule.result, workflows, path)
					return result
				}
				if max[2] > rule.value {
					result += routes([]int{min[0], min[1], rule.value + 1, min[3]}, max, rule.result, workflows, path)
					max = []int{max[0], max[1], rule.value, max[3]}
				}
			}
		} else if rule.part == 's' {
			if rule.lessThan {
				if max[3] < rule.value {
					result += routes(min, max, rule.result, workflows, path)
					return result
				}
				if min[3] < rule.value {
					result += routes(min, []int{max[0], max[1], max[2], rule.value - 1}, rule.result, workflows, path)
					min = []int{min[0], min[1], min[2], rule.value}
				}
			} else {
				if min[3] > rule.value {
					result += routes(min, max, rule.result, workflows, path)
					return result
				}
				if max[3] > rule.value {
					result += routes([]int{min[0], min[1], min[2], rule.value + 1}, max, rule.result, workflows, path)
					max = []int{max[0], max[1], max[2], rule.value}
				}
			}
		} else {
			result += routes(min, max, rule.result, workflows, path)
			return result
		}
	}

	return result
}
