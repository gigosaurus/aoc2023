package day6

import (
	_ "embed"
	"math"
	"strconv"
	"strings"
)

//go:embed input.txt
var input string

// example: 3, real: 4
var numberOfRaces = 4

func Part1() int {
	times := make([]int, numberOfRaces)
	records := make([]int, numberOfRaces)
	i := 0
	readingTimes := true
	for _, line := range strings.Split(input, "\r\n") {
		for _, split := range strings.Split(line, " ") {
			if split != "" && split[0] != 'T' && split[0] != 'D' {
				if readingTimes {
					times[i], _ = strconv.Atoi(split)
				} else {
					records[i], _ = strconv.Atoi(split)
				}
				i++
			}
		}
		readingTimes = false
		i = 0
	}

	total := 1
	for i = 0; i < numberOfRaces; i++ {
		d := float64(times[i]*times[i] - 4*records[i])
		root1 := int(math.Floor((math.Sqrt(d) - float64(times[i])) / -2))
		root2 := int(math.Ceil((-math.Sqrt(d) - float64(times[i])) / -2))
		total *= root2 - root1 - 1
	}
	return total
}

func Part2() int {
	times := ""
	records := ""
	readingTimes := true
	for _, line := range strings.Split(input, "\r\n") {
		for _, split := range strings.Split(line, " ") {
			if split != "" && split[0] != 'T' && split[0] != 'D' {
				if readingTimes {
					times += split
				} else {
					records += split
				}
			}
		}
		readingTimes = false
	}

	timesS, _ := strconv.Atoi(times)
	recordsS, _ := strconv.Atoi(records)
	d := float64(timesS*timesS - 4*recordsS)
	root1 := int(math.Floor((math.Sqrt(d) - float64(timesS)) / -2))
	root2 := int(math.Ceil((-math.Sqrt(d) - float64(timesS)) / -2))
	return root2 - root1 - 1
}
