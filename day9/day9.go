package day9

import (
	_ "embed"
	"strconv"
	"strings"
)

//go:embed input.txt
var input string

func Part1() int {
	lines := strings.Split(input, "\r\n")
	total := 0
	for _, line := range lines {
		numberString := strings.Split(line, " ")
		numbers := make([]int, len(numberString))
		for i, v := range numberString {
			numbers[i], _ = strconv.Atoi(v)
		}

		sum := numbers[len(numbers)-1]
		for {
			nextRow := make([]int, len(numbers)-1)
			allZero := true
			for i := 0; i < len(numbers)-1; i++ {
				diff := numbers[i+1] - numbers[i]
				nextRow[i] = diff
				if diff != 0 {
					allZero = false
				}
			}
			if allZero {
				total += sum
				break
			}
			sum += nextRow[len(nextRow)-1]
			numbers = nextRow
		}
	}

	return total
}

func Part2() int {
	lines := strings.Split(input, "\r\n")
	total := 0

	for _, line := range lines {
		numberString := strings.Split(line, " ")
		numbers := make([]int, len(numberString))
		for i, v := range numberString {
			numbers[i], _ = strconv.Atoi(v)
		}

		firstNum := numbers[0]
		sum := 0
		for {
			nextRow := make([]int, len(numbers)-1)
			allZero := true
			for i := 0; i < len(numbers)-1; i++ {
				diff := numbers[i] - numbers[i+1]
				nextRow[i] = diff
				if diff != 0 {
					allZero = false
				}
			}
			if allZero {
				total += firstNum + sum
				break
			}
			sum += nextRow[0]
			numbers = nextRow
		}
	}

	return total
}

func diffAtDepth2(depth int, numbers []int) int {
	if depth == 0 {
		return numbers[1] - numbers[0]
	}
	return diffAtDepth2(depth-1, numbers) - diffAtDepth2(depth-1, numbers[1:])
}
