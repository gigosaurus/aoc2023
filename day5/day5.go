package day5

import (
	_ "embed"
	"slices"
	"strconv"
	"strings"
)

//go:embed input.txt
var input string

// example: 4, real: 20
var seedCount = 20

func Part1() int {
	seeds := make([]int, seedCount)
	moved := make([]bool, seedCount)
	lastBlank := 0
	for y, line := range strings.Split(input, "\r\n") {
		dest := 0
		source := 0
		length := 0
		for x, part := range strings.Split(line, " ") {
			if y == 0 {
				if x > 0 {
					seeds[x-1], _ = strconv.Atoi(part)
				}
			} else if x == 0 && part == "" {
				lastBlank = 0
				for i := 0; i < len(moved); i++ {
					moved[i] = false
				}
			} else if lastBlank > 1 {
				if x == 0 {
					dest, _ = strconv.Atoi(part)
				} else if x == 1 {
					source, _ = strconv.Atoi(part)
				} else if x == 2 {
					length, _ = strconv.Atoi(part)
				}
			}
		}

		if length > 0 {
			for i := 0; i < len(seeds); i++ {
				if moved[i] == false && seeds[i] >= source && seeds[i] <= source+length {
					seeds[i] = dest + seeds[i] - source
					moved[i] = true
				}
			}
		}

		lastBlank++
	}

	lowest := seeds[0]
	for _, s := range seeds {
		if s < lowest {
			lowest = s
		}
	}

	return lowest
}

type Range struct {
	from int
	to   int
}

type AlmanacMapRow struct {
	r Range
	m int
}

type AlmanacMap struct {
	rows []AlmanacMapRow
}

func (a *AlmanacMap) Sort() {
	sortedRows := make([]AlmanacMapRow, len(a.rows))
	from := make([]int, len(a.rows))
	for r := 0; r < len(a.rows); r++ {
		from[r] = a.rows[r].r.from
	}
	slices.Sort(from)
	for r := 0; r < len(a.rows); r++ {
		sortedRows[slices.Index(from, a.rows[r].r.from)] = a.rows[r]
	}

	a.rows = sortedRows
}

func (a *AlmanacMap) Combine(other *AlmanacMap) *AlmanacMap {
	var combined []AlmanacMapRow
	for _, ar := range a.rows {
		aJ := Range{ar.r.from + ar.m, ar.r.to + ar.m}
		added := false
		carryOver := AlmanacMapRow{Range{0, 0}, 0}
		for _, or := range other.rows {
			if carryOver.r.from > 0 && carryOver.r.to > 0 {
				if or.r.from-ar.m > carryOver.r.from {
					combined = append(combined, AlmanacMapRow{Range{carryOver.r.from, or.r.from - 1}, carryOver.m})
				}
				aJ = Range{carryOver.r.from + ar.m, carryOver.r.to + ar.m}
				carryOver = AlmanacMapRow{Range{0, 0}, 0}
			}
			if aJ.from <= or.r.to && aJ.to >= or.r.from { // overlap
				added = true
				if aJ.from < or.r.from {
					// starts before
					if aJ.to <= or.r.to {
						// ends during, aJ.from < or.r.from < aJ.to < or.r.to
						combined = append(combined, AlmanacMapRow{Range{aJ.from - ar.m, or.r.from - ar.m - 1}, ar.m})
						combined = append(combined, AlmanacMapRow{Range{or.r.from - ar.m, aJ.to - ar.m}, ar.m + or.m})
					} else if aJ.to > or.r.to {
						// ends after, aJ.from < or.r.from < or.r.to < aJ.to
						combined = append(combined, AlmanacMapRow{Range{aJ.from - ar.m, or.r.from - ar.m - 1}, ar.m})
						combined = append(combined, AlmanacMapRow{Range{or.r.from - ar.m, or.r.to - ar.m}, ar.m + or.m})
						combined = append(combined, AlmanacMapRow{Range{or.r.to - ar.m + 1, aJ.to - ar.m}, ar.m})
					}
				} else if aJ.from >= or.r.from {
					// starts after
					if aJ.to <= or.r.to {
						// totally inside, or.r.from < aJ.from < aJ.to < or.r.to
						combined = append(combined, AlmanacMapRow{Range{aJ.from - ar.m, aJ.to - ar.m}, ar.m + or.m})
					} else if aJ.to > or.r.to {
						// ends after, or.r.from < aJ.from < or.r.to < aJ.to
						combined = append(combined, AlmanacMapRow{Range{aJ.from - ar.m, or.r.to - ar.m}, ar.m + or.m})
						carryOver = AlmanacMapRow{Range{or.r.to - ar.m + 1, aJ.to - ar.m}, ar.m}
					}
				}
			}
		}

		if carryOver.r.from > 0 && carryOver.r.to > 0 {
			combined = append(combined, carryOver)
			carryOver = AlmanacMapRow{Range{0, 0}, 0}
		}

		if !added {
			combined = append(combined, ar)
		}
	}

	return &AlmanacMap{combined}
}

func Part2() int {
	var seeds []Range
	var almanac []*AlmanacMap
	var almanacRows []AlmanacMapRow

	lastBlank := 0
	for y, line := range strings.Split(input, "\r\n") {
		lastSeed := -1
		dest := 0
		source := 0
		for x, part := range strings.Split(line, " ") {
			if y == 0 {
				if x > 0 {
					if lastSeed == -1 {
						lastSeed, _ = strconv.Atoi(part)
					} else {
						seedLength, _ := strconv.Atoi(part)
						seeds = append(seeds, Range{lastSeed, lastSeed + seedLength - 1})
						lastSeed = -1
					}
				}
			} else if x == 0 && part == "" {
				lastBlank = 0
				if len(almanacRows) > 0 {
					newMap := &AlmanacMap{almanacRows}
					newMap.Sort()
					almanac = append(almanac, newMap)
					almanacRows = make([]AlmanacMapRow, 0)
				}
			} else if lastBlank > 1 {
				if x == 0 {
					dest, _ = strconv.Atoi(part)
				} else if x == 1 {
					source, _ = strconv.Atoi(part)
				} else if x == 2 {
					length, _ := strconv.Atoi(part)
					almanacRows = append(almanacRows, AlmanacMapRow{Range{source, source + length - 1}, dest - source})
					dest = 0
					source = 0
				}
			}
		}

		lastBlank++
	}

	newMap := &AlmanacMap{almanacRows}
	newMap.Sort()
	almanac = append(almanac, newMap)

	seedMapRows := make([]AlmanacMapRow, len(seeds))
	for i := 0; i < len(seeds); i++ {
		seedMapRows[i] = AlmanacMapRow{seeds[i], 0}
	}

	combined := &AlmanacMap{seedMapRows}
	combined.Sort()
	for i := 0; i < len(almanac); i++ {
		combined = combined.Combine(almanac[i])
	}

	lowest := combined.rows[0].r.from + combined.rows[0].m
	for i := 1; i < len(combined.rows); i++ {
		if lowest > combined.rows[i].r.from+combined.rows[i].m {
			lowest = combined.rows[i].r.from + combined.rows[i].m
		}
	}

	return lowest
}
