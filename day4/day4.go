package day4

import (
	_ "embed"
	"strconv"
	"strings"
)

//go:embed input.txt
var input string

// example: 5, real: 10
var winningNumbersCount = 10

// example: 6, real: 220
var cardCount = 220

func Part1() int {
	total := 0
	for _, line := range strings.Split(input, "\r\n") {
		winningNumbers := make([]int, winningNumbersCount)
		i := 0
		score := 0
		for _, part := range strings.Split(line, " ") {
			if part != "" {
				if i >= 2 {
					if i >= 2+winningNumbersCount {
						number, _ := strconv.Atoi(part)
						for _, n := range winningNumbers {
							if number == n {
								if score == 0 {
									score = 1
								} else {
									score <<= 1
								}
							}
						}
					} else if i-2 != winningNumbersCount {
						winningNumbers[i-2], _ = strconv.Atoi(part)
					}
				}
				i++
			}
		}
		total += score
	}

	return total
}

func Part2() int {
	total := cardCount
	cards := make([]int, cardCount)
	for c, line := range strings.Split(input, "\r\n") {
		winningNumbers := make([]int, winningNumbersCount)
		i := 0
		score := 0
		for _, part := range strings.Split(line, " ") {
			if part != "" {
				if i >= 2 {
					if i >= 2+winningNumbersCount {
						number, _ := strconv.Atoi(part)
						for _, n := range winningNumbers {
							if number == n {
								score++
							}
						}
					} else if i-2 != winningNumbersCount {
						winningNumbers[i-2], _ = strconv.Atoi(part)
					}
				}
				i++
			}
		}
		for s := 0; s < score; s++ {
			cards[c+s+1] += 1 + cards[c]
			total += 1 + cards[c]
		}
	}

	return total
}
