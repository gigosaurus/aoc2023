package day17

import (
	_ "embed"
	"strings"
)

//go:embed input.txt
var input string

// example: 13, real: 141
var length = 141

type Point struct {
	x   int
	y   int
	dir int
	c   int
	v   int
}

type Points struct {
	points []*Point
}

func (p *Points) add(x int, y int, dir int, c int, v int) {
	for i := 0; i < len(p.points); i++ {
		if p.points[i].x == x && p.points[i].y == y && p.points[i].dir == dir && p.points[i].c == c {
			return
		}
		if p.points[i].v > v {
			p.points = append(p.points[:i+1], p.points[i:]...)
			p.points[i] = &Point{x, y, dir, c, v}
			return
		}
	}

	p.points = append(p.points, &Point{x, y, dir, c, v})
}

func (p *Points) get() *Point {
	point := p.points[0]
	p.points = p.points[1:]
	return point
}

func (p *Points) empty() bool {
	return p.points == nil || len(p.points) == 0
}

func Part1() int {
	return path(0, 3)
}

func Part2() int {
	return path(4, 10)
}

func path(min int, max int) int {

	lines := strings.Split(input, "\r\n")
	blocks := make([][]int, length)
	dist := make([][][][]int, length)
	p := &Points{}
	for i := 0; i < len(lines); i++ {
		blocks[i] = make([]int, length)
		dist[i] = make([][][]int, length)
		for j, c := range lines[i] {
			blocks[i][j] = int(c - '0')
			dist[i][j] = make([][]int, 4)
			for k := 0; k < 4; k++ {
				dist[i][j][k] = make([]int, max)
				for l := 0; l < max; l++ {
					dist[i][j][k][l] = 999999
				}
			}
		}
	}

	// 0 = up, 1 = down, 2 = left, 3 = right
	p.add(0, 0, 3, 1, 0)
	p.add(0, 0, 1, 1, 0)

	point := &Point{}

	for {
		if p.empty() {
			break
		}

		point = p.get()

		if dist[point.y][point.x][point.dir][point.c-1] < point.v {
			continue
		}
		dist[point.y][point.x][point.dir][point.c-1] = point.v
		if point.y == length-1 && point.x == length-1 {
			break
		}
		if point.dir != 1 && point.y > 0 {
			if point.dir == 0 {
				if point.c < max {
					p.add(point.x, point.y-1, 0, point.c+1, point.v+blocks[point.y-1][point.x])
				}
			} else if point.c >= min {
				p.add(point.x, point.y-1, 0, 1, point.v+blocks[point.y-1][point.x])
			}
		}

		if point.dir != 0 && point.y+1 < length {
			if point.dir == 1 {
				if point.c < max {
					p.add(point.x, point.y+1, 1, point.c+1, point.v+blocks[point.y+1][point.x])
				}
			} else if point.c >= min {
				p.add(point.x, point.y+1, 1, 1, point.v+blocks[point.y+1][point.x])
			}
		}

		if point.dir != 3 && point.x > 0 {
			if point.dir == 2 {
				if point.c < max {
					p.add(point.x-1, point.y, 2, point.c+1, point.v+blocks[point.y][point.x-1])
				}
			} else if point.c >= min {
				p.add(point.x-1, point.y, 2, 1, point.v+blocks[point.y][point.x-1])
			}
		}

		if point.dir != 2 && point.x+1 < length {
			if point.dir == 3 {
				if point.c < max {
					p.add(point.x+1, point.y, 3, point.c+1, point.v+blocks[point.y][point.x+1])
				}
			} else if point.c >= min {
				p.add(point.x+1, point.y, 3, 1, point.v+blocks[point.y][point.x+1])
			}
		}
	}

	shortest := 999999
	for i := 0; i < max; i++ {
		if shortest > dist[length-1][length-1][1][i] {
			shortest = dist[length-1][length-1][1][i]
		}
		if shortest > dist[length-1][length-1][3][i] {
			shortest = dist[length-1][length-1][3][i]
		}
	}

	return shortest
}
