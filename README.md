# Advent of Code 2023
See [Advent of Code 2023](https://adventofcode.com/2023)

My solutions are written in Go. I've opted for execution speed over readability, and I'm still learning the language, so the code is not very idiomatic.

## Execution Times
```
37.4µs          Trebuchet
306µs
94.6µs          Cube Conundrum
153µs
113µs           Gear Ratios
109µs
142µs           Scratchcards
119µs
20.8µs          If You Give A Seed A Fertilizer
67.5µs
0.354µs         Wait For It
0.400µs
164µs           Camel Cards
165µs
72.3µs          Haunted Wasteland
264µs
115µs           Mirage Maintenance
134µs
38.8µs          Pipe Maze
88.4µs
99.0µs          Cosmic Expansion
101µs
573µs           Hot Springs
5340µs
59.9µs          Point of Incidence
60.0µs
25.6µs          Parabolic Reflector Dish
6332µs
32.7µs          Lens Library
99.8µs
38.4µs          The Floor Will Be Lava
11129µs
942450µs                Clumsy Crucible
7186714µs
46.8µs          Lavaduct Lagoon
55.8µs
190µs           Aplenty
215µs
3258µs          Pulse Propagation
14618µs
23292µs         Step Counter
5425134µs
574µs           Sand Slabs
4251µs
----------------
13626ms
```
