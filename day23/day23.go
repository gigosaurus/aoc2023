package day23

import (
	_ "embed"
	"slices"
	"strings"
)

//go:embed input.txt
var input string

// example: 22, real: 140
var goalY = 140

var dirs = [][]int{{0, 1, 0}, {0, -1, 1}, {1, 0, 2}, {-1, 0, 3}}

type Trail struct {
	fromX    int
	fromY    int
	toX      int
	toY      int
	distance int
}

type Paths struct {
	trails []*Trail
}

type Node struct {
	fromId   int
	toId     int
	weight   int
	children []*Node
}

func Part1() int {
	lines := strings.Split(input, "\r\n")

	trails := traverse(lines, 1, 1, 1, 0, 1, 0, 1)
	graph := trailsToDiGraph(trails)
	return longest(graph)
}

func trailsToDiGraph(trails []*Trail) *Node {
	nodeMap := make(map[int]map[int]*Node)
	for _, trail := range trails {
		fromId := trail.fromY<<8 | trail.fromX
		toId := trail.toY<<8 | trail.toX
		if nodeMap[fromId] == nil {
			nodeMap[fromId] = make(map[int]*Node)
		}
		old := nodeMap[fromId][toId]
		if old == nil {
			nodeMap[fromId][toId] = &Node{fromId, toId, trail.distance, make([]*Node, 0)}
		} else if trail.distance > old.weight {
			old.weight = trail.distance
		}
	}

	nodeArrays := make(map[int][]*Node)
	for k, nodes := range nodeMap {
		nodeArray := make([]*Node, 0)
		for _, toNode := range nodes {
			nodeArray = append(nodeArray, toNode)
		}
		nodeArrays[k] = nodeArray
	}

	for _, nodes := range nodeArrays {
		for _, node := range nodes {
			node.children = nodeArrays[node.toId]
		}

	}

	return nodeArrays[1][0]
}

func longest(node *Node) int {
	m := 0
	for _, child := range node.children {
		v := longest(child)
		if m == 0 || v > m {
			m = v
		}
	}

	return node.weight + m
}

func traverse(island []string, x int, y int, prevX int, prevY int, fromX int, fromY int, distance int) []*Trail {
	trails := make([]*Trail, 0)
	for _, dir := range dirs {
		if prevX != x+dir[1] || prevY != y+dir[0] {
			tile := island[y+dir[0]][x+dir[1]]
			if tile == '.' {
				if y+dir[0] == goalY {
					trails = append(trails, &Trail{fromX, fromY, x, y + dir[0], distance + 1})
				} else {
					trails = append(trails, traverse(island, x+dir[1], y+dir[0], x, y, fromX, fromY, distance+1)...)
				}
			} else if dir[2] == 0 && tile == '>' || dir[2] == 1 && tile == '<' || dir[2] == 2 && tile == 'v' || dir[2] == 3 && tile == '^' {
				trails = append(trails, handleSplit(island, x+dir[1]*2, y+dir[0]*2, x+dir[1], y+dir[0], fromX, fromY, distance+2)...)
			}
		}
	}

	return trails
}

func handleSplit(island []string, x int, y int, prevX int, prevY int, fromX int, fromY int, distance int) []*Trail {
	trails := make([]*Trail, 0)
	trails = append(trails, &Trail{fromX, fromY, x, y, distance})
	for _, dir := range dirs {
		if prevX != x+dir[1] || prevY != y+dir[0] {
			tile := island[y+dir[0]][x+dir[1]]
			if tile == '>' || tile == '<' || tile == '^' || tile == 'v' {
				trails = append(trails, traverse(island, x+dir[1]*2, y+dir[0]*2, x+dir[1], y+dir[0], x, y, 2)...)
			}
		}
	}

	return trails
}

func Part2() int {
	lines := strings.Split(input, "\r\n")

	trails := traverse(lines, 1, 1, 1, 0, 1, 0, 1)
	graph := trailsToGraph(trails)
	return longest2(graph, []int{graph.fromId, graph.toId})
}

func trailsToGraph(trails []*Trail) *Node {
	nodeMap := make(map[int]map[int]*Node)
	for _, trail := range trails {
		fromId := trail.fromY<<8 | trail.fromX
		toId := trail.toY<<8 | trail.toX
		if nodeMap[fromId] == nil {
			nodeMap[fromId] = make(map[int]*Node)
		}
		if nodeMap[toId] == nil {
			nodeMap[toId] = make(map[int]*Node)
		}
		old := nodeMap[fromId][toId]
		if old == nil {
			nodeMap[fromId][toId] = &Node{fromId, toId, trail.distance, make([]*Node, 0)}
			nodeMap[toId][fromId] = &Node{toId, fromId, trail.distance, make([]*Node, 0)}
		} else if trail.distance > old.weight {
			old.weight = trail.distance
		}
	}

	nodeArrays := make(map[int][]*Node)
	for k, nodes := range nodeMap {
		nodeArray := make([]*Node, 0)
		for _, toNode := range nodes {
			nodeArray = append(nodeArray, toNode)
		}
		nodeArrays[k] = nodeArray
	}

	for _, nodes := range nodeArrays {
		for _, node := range nodes {
			node.children = nodeArrays[node.toId]
		}
	}

	return nodeArrays[1][0]
}

func longest2(node *Node, visited []int) int {
	m := 0
	end := true
	for _, child := range node.children {
		if slices.Index(visited, child.toId) == -1 {
			end = false
			v := longest2(child, append(visited, child.toId))
			if m == 0 || v > m {
				m = v
			}
		}
	}

	if end {
		if node.toId == 140<<8|139 {
			return node.weight + m
		}
		return -999999
	}

	return node.weight + m
}
