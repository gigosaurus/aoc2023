package day8

import (
	_ "embed"
	"strings"
)

//go:embed input.txt
var input string

func Part1() int {
	lines := strings.Split(input, "\r\n")
	directions := lines[0]
	sources := make([]int, 26<<10)
	destL := make([]int, len(lines)-2)
	destR := make([]int, len(lines)-2)
	for y, line := range lines {
		if y > 1 {
			parts := strings.Split(line, " = ")
			sources[pathToInt(parts[0])] = y - 2
			destL[y-2] = pathToInt(parts[1][1:4])
			destR[y-2] = pathToInt(parts[1][6:9])
		}
	}

	i := 0
	p := 0
	for {
		if p == 25<<10+25<<5+25 {
			return i
		}

		d := directions[i%len(directions)]
		pi := sources[p]
		if d == 76 {
			p = destL[pi]
		} else {
			p = destR[pi]
		}
		i++
	}
}

func Part2() int {
	var ghosts []int
	lines := strings.Split(input, "\r\n")
	directions := lines[0]
	sources := make([]int, 26<<10)
	destL := make([]int, len(lines)-2)
	destR := make([]int, len(lines)-2)
	for y, line := range lines {
		if y > 1 {
			parts := strings.Split(line, " = ")
			source := pathToInt(parts[0])
			sources[source] = y - 2
			destL[y-2] = pathToInt(parts[1][1:4])
			destR[y-2] = pathToInt(parts[1][6:9])
			if source < 1<<10 {
				ghosts = append(ghosts, source)
			}
		}
	}

	total := 0
	for _, ghost := range ghosts {
		length := findGhostPathLength(ghost, directions, sources, destL, destR)
		if total == 0 {
			total = length
		} else {
			total = lcm(total, length)
		}
	}

	return total
}

func findGhostPathLength(start int, directions string, sources []int, destL []int, destR []int) int {
	i := 0
	p := start
	for {
		if p >= 25<<10 {
			return i
		}

		pi := sources[p]
		if directions[i%len(directions)] == 76 {
			p = destL[pi]
		} else {
			p = destR[pi]
		}
		i++
	}
}

func pathToInt(path string) int {
	return int(path[2]-65)<<10 + int(path[1]-65)<<5 + int(path[0]-65)
}

func hcf(a int, b int) int {
	if b == 0 {
		return a
	}
	return hcf(b, a%b)
}

func lcm(a int, b int) int {
	return a * b / hcf(a, b)
}
