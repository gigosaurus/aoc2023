package day20

import (
	_ "embed"
	"strings"
)

//go:embed input.txt
var input string

type FlipFlop struct {
	on      bool
	outputs []string
}

type Conjunction struct {
	inputs  []string
	high    []bool
	outputs []string
}

type Pulse struct {
	input  string
	output string
	high   bool
}

func (c *Conjunction) Pulse(input string, high bool) {
	for i := range c.inputs {
		if c.inputs[i] == input {
			c.high[i] = high
			return
		}
	}
}

func (c *Conjunction) High() bool {
	for _, high := range c.high {
		if !high {
			return false
		}
	}

	return true
}

func Part1() int {
	flipflops := make(map[string]*FlipFlop)
	conjunctions := make(map[string]*Conjunction)
	inputs := make(map[string][]string)
	var broadcast []string
	for _, line := range strings.Split(input, "\r\n") {
		parts := strings.SplitN(line, " -> ", 2)
		outputs := strings.Split(parts[1], ", ")
		if parts[0][0] == '%' {
			flipflops[parts[0][1:]] = &FlipFlop{false, outputs}
		} else if parts[0][0] == '&' {
			conjunctions[parts[0][1:]] = &Conjunction{[]string{}, []bool{}, outputs}
		} else {
			broadcast = outputs
			parts[0] = "." + parts[0]
		}
		for _, output := range outputs {
			inputs[output] = append(inputs[output], parts[0][1:])
		}
	}

	for k, v := range inputs {
		if conjunctions[k] == nil {
			continue
		}
		conjunctions[k].inputs = v
		conjunctions[k].high = make([]bool, len(v))
	}

	lowCount := 0
	highCount := 0
	pulses := make([]*Pulse, len(broadcast))
	for j, inp := range broadcast {
		pulses[j] = &Pulse{"button", inp, false}
	}
	for i := 0; i < 1000; i++ {
		lowCount++
		lowC, highC := process(pulses, flipflops, conjunctions)
		lowCount += lowC
		highCount += highC
	}

	return lowCount * highCount
}

func process(pulses []*Pulse, flipFlops map[string]*FlipFlop, conjunctions map[string]*Conjunction) (int, int) {
	lowCount := 0
	highCount := 0
	var nextPulses []*Pulse
	for {
		if len(pulses) == 0 {
			return lowCount, highCount
		}

		for _, p := range pulses {
			if p.high {
				highCount++
			} else {
				lowCount++
			}
			if flipFlops[p.output] != nil {
				if p.high {
					continue
				}

				flipFlops[p.output].on = !flipFlops[p.output].on
				for _, output := range flipFlops[p.output].outputs {
					nextPulses = append(nextPulses, &Pulse{p.output, output, flipFlops[p.output].on})
				}
			} else if conjunctions[p.output] != nil {
				conjunctions[p.output].Pulse(p.input, p.high)
				sendHigh := !conjunctions[p.output].High()
				for _, output := range conjunctions[p.output].outputs {
					nextPulses = append(nextPulses, &Pulse{p.output, output, sendHigh})
				}
			}
		}

		pulses = nextPulses
		nextPulses = []*Pulse{}
	}
}

func Part2() int {
	flipflops := make(map[string]*FlipFlop)
	conjunctions := make(map[string]*Conjunction)
	inputs := make(map[string][]string)
	var broadcast []string
	for _, line := range strings.Split(input, "\r\n") {
		parts := strings.SplitN(line, " -> ", 2)
		outputs := strings.Split(parts[1], ", ")
		if parts[0][0] == '%' {
			flipflops[parts[0][1:]] = &FlipFlop{false, outputs}
		} else if parts[0][0] == '&' {
			conjunctions[parts[0][1:]] = &Conjunction{[]string{}, []bool{}, outputs}
		} else {
			broadcast = outputs
			parts[0] = "." + parts[0]
		}
		for _, output := range outputs {
			inputs[output] = append(inputs[output], parts[0][1:])
		}
	}

	for k, v := range inputs {
		if conjunctions[k] == nil {
			continue
		}
		conjunctions[k].inputs = v
		conjunctions[k].high = make([]bool, len(v))
	}

	// assumption: "rx" is received by a single conjunction
	// assumption: input is crafted so that multiplying the conjunction's inputs gives the answer
	conj := conjunctions[inputs["rx"][0]]

	pulses := make([]*Pulse, len(broadcast))
	for j, inp := range broadcast {
		pulses[j] = &Pulse{"button", inp, false}
	}
	i := 0
	is := make([]int, len(conj.inputs))
	for {
		i++
		found := findRx(pulses, flipflops, conjunctions)
		if found >= 0 {
			is[found] = i
			total := 1
			for _, v := range is {
				total *= v
			}
			if total > 0 {
				return total
			}
		}
	}
}

func findRx(pulses []*Pulse, flipFlops map[string]*FlipFlop, conjunctions map[string]*Conjunction) int {
	lowCount := 0
	highCount := 0
	toReturn := -1
	var nextPulses []*Pulse
	for {
		if len(pulses) == 0 {
			return toReturn
		}

		for _, p := range pulses {
			if p.high {
				highCount++
			} else {
				lowCount++
			}

			if flipFlops[p.output] != nil {
				if p.high {
					continue
				}

				flipFlops[p.output].on = !flipFlops[p.output].on
				for _, output := range flipFlops[p.output].outputs {
					nextPulses = append(nextPulses, &Pulse{p.output, output, flipFlops[p.output].on})
				}
			} else if conjunctions[p.output] != nil {
				conjunctions[p.output].Pulse(p.input, p.high)
				if conjunctions[p.output].outputs[0] == "rx" && p.high {
					for k := range conjunctions[p.output].inputs {
						if conjunctions[p.output].inputs[k] == p.input {
							toReturn = k
							break
						}
					}
				}
				sendHigh := !conjunctions[p.output].High()
				for _, output := range conjunctions[p.output].outputs {
					nextPulses = append(nextPulses, &Pulse{p.output, output, sendHigh})
				}
			}
		}

		pulses = nextPulses
		nextPulses = []*Pulse{}
	}
}
