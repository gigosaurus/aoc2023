package day3

import (
	"aoc2023/utils"
	_ "embed"
	"strconv"
	"strings"
)

//go:embed input.txt
var input string

type Coordinates struct {
	x int
	y int
	n int // 0 = symbol
}

func Part1() int {
	lines := strings.Split(input, "\n")
	parts := findSymbols(lines)

	var numbers []Coordinates
	for _, v := range parts { // check 3x3 centered around each symbol
		minY := utils.Max(v.y-1, 0)
		maxY := utils.Min(v.y+1, len(lines)-1)
		for y := minY; y <= maxY; y++ {
			line := lines[y]
			minX := utils.Max(v.x-1, 0)
			maxX := utils.Min(v.x+1, len(line)-1)
			for x := minX; x <= maxX; x++ {
				if !(x == v.x && y == v.y) { // ignore symbol
					char := line[x]
					if char >= 48 && char <= 57 { // found a number
						num, newX := getNum(line, x)
						x = newX // skip over the rest of the number
						numbers = append(numbers, Coordinates{x: x, y: y, n: num})
					}
				}
			}
		}
	}

	parts = append(parts, numbers...)

	total := 0
	for _, v := range parts {
		total += v.n
	}

	return total
}

func getNum(line string, x int) (int, int) {
	full := ""
	full += string(line[x])
	for c := x - 1; c >= 0; c-- {
		if line[c] >= 48 && line[c] <= 57 {
			full = string(line[c]) + full
		} else {
			break
		}
	}

	for c := x + 1; c < len(line); c++ {
		if line[c] >= 48 && line[c] <= 57 {
			full += string(line[c])
		} else {
			x = c
			break
		}
	}

	num, _ := strconv.Atoi(full)
	return num, x
}

func findSymbols(lines []string) []Coordinates {
	var parts []Coordinates
	for y, line := range lines {
		for x, char := range line {
			if char >= 33 && char <= 45 || char == 47 || char >= 58 && char <= 126 {
				parts = append(parts, Coordinates{x: x, y: y, n: 0})
			}
		}
	}

	return parts
}

func Part2() int {
	lines := strings.Split(input, "\n")
	parts := findSymbols(lines)

	ratioSum := 0
	for _, v := range parts {
		minY := utils.Max(v.y-1, 0)
		maxY := utils.Min(v.y+1, len(lines)-1)
		partNumbers := make([]Coordinates, 2)
		pi := 0
		for y := minY; y <= maxY; y++ { // check 3x3 centered around each symbol
			if pi >= 2 { // found more than 2
				break
			}
			line := lines[y]
			minX := utils.Max(v.x-1, 0)
			maxX := utils.Min(v.x+1, len(line)-1)
			for x := minX; x <= maxX; x++ {
				if pi >= 2 { // found more than 2
					break
				}
				if !(x == v.x && y == v.y) { // ignore symbol
					char := line[x]
					if char >= 48 && char <= 57 { // found a number
						num, newX := getNum(line, x)
						x = newX
						partNumbers[pi] = Coordinates{x: x, y: y, n: num}
						pi++
					}
				}
			}
		}
		if pi == 2 { // need exactly 2
			ratioSum += partNumbers[0].n * partNumbers[1].n
		}
	}

	return ratioSum
}
